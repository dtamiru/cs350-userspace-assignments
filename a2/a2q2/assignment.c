#include "assignment.h"

// Condition : #consumers < 2 * #producers AND #consumers >= 0 AND #producers >= 0

void
consume_enter(struct resource *resource)
{
    pthread_mutex_lock(&resource->mutex); // acquire resource lock

    while (resource->num_producers * resource->ratio < resource->num_consumers + 1) {
        pthread_cond_wait(&resource->cond, &resource->mutex); // wait until condition would be met
    }
    
    resource->num_consumers++; // add consumer

    pthread_cond_signal(&resource->cond); // signal to others that the condition was met
    
    // don't release lock yet because it is needed after this function call
}

void
consume_exit(struct resource *resource)
{
    // lock already acquired

    while (resource->num_consumers - 1 < 0) {
        pthread_cond_wait(&resource->cond, &resource->mutex); // wait until condition would be met
    }

    resource->num_consumers--; // remove consumer

    pthread_cond_signal(&resource->cond); // signal to others that the condition was met

    pthread_mutex_unlock(&resource->mutex); // release lock
}

void
produce_enter(struct resource *resource)
{
    pthread_mutex_lock(&resource->mutex); // acquire resource lock

    resource->num_producers++; // add producer

    pthread_cond_signal(&resource->cond); // condition must be met - signal to others

    // don't release lock yet because it is needed after this function call
}

void
produce_exit(struct resource *resource)
{
    // lock already acquired

    while (resource->num_producers - 1 < 0 || (resource->num_producers - 1) * resource->ratio < resource->num_consumers) {
        pthread_cond_wait(&resource->cond, &resource->mutex); // wait until condition would be met
    }

    resource->num_producers--; // remove producer

    pthread_cond_signal(&resource->cond); // condition must be met - signal to others

    pthread_mutex_unlock(&resource->mutex); // release lock
}


