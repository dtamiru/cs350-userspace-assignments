/* map.c
 * ----------------------------------------------------------
 *  CS350
 *  Assignment 1
 *  Question 1
 *
 *  Purpose:  Gain experience with threads and basic
 *  synchronization.
 *
 *  YOU MAY ADD WHATEVER YOU LIKE TO THIS FILE.
 *  YOU CANNOT CHANGE THE SIGNATURE OF MultithreadedWordCount.
 * ----------------------------------------------------------
 */
#include "data.h"

#include <stdlib.h>
#include <string.h>

pthread_mutex_t count_mutex;
long long count;

// Thread Arguments
struct Arg {
  struct Library *lib;
  char *word;
  unsigned int idx;
};

void *workerWordCount(void *arg) {
  // * much of this code is copied is copied over from singleThreadedWordCount in main.c *
  struct Arg* w_arg = (struct Arg*) arg;

  int i = w_arg->idx * w_arg->lib->numArticles/NUMTHREADS;
  int end = (w_arg->idx + 1) * w_arg->lib->numArticles/NUMTHREADS;

  size_t wordCount = 0;

  for (; i < end && i < w_arg->lib->numArticles; i++) {
    struct Article * art = w_arg->lib->articles[i];

    for ( unsigned int j = 0; j < art->numWords; j++) {
        // Get the length of the function.
        size_t len = strnlen( art->words[j], MAXWORDSIZE );
        if (!strncmp( art->words[j], w_arg->word, len )) {
          wordCount++;
        }
    }
  }
  
  // acquire global word count for lock/mutex and increment
  pthread_mutex_lock(&count_mutex);
  count += wordCount;
  pthread_mutex_unlock(&count_mutex);

  return 0;
}

/* --------------------------------------------------------------------
 * MultithreadedWordCount
 * --------------------------------------------------------------------
 * Takes a Library of articles containing words and a word.
 * Returns the total number of times that the word appears in the
 * Library.
 *
 * For example, "There the thesis sits on the theatre bench.", contains
 * 2 occurences of the word "the".
 * --------------------------------------------------------------------
 */

size_t MultithreadedWordCount( struct  Library * lib, char * word)
{
  printf("Parallelizing with %d threads...\n", NUMTHREADS);

  // number of threads should at most the number of article (otherwise, wasting threads)
  if (lib->numArticles < NUMTHREADS)
    NUMTHREADS = lib->numArticles;

  // declare threads
  pthread_t workers[NUMTHREADS];
  struct Arg args[NUMTHREADS];

  // create threads
  for (int idx = 0; idx < NUMTHREADS; ++idx) {
   
    args[idx].idx = idx;
    args[idx].lib = lib;
    args[idx].word = word;
    
    pthread_create(&workers[idx], NULL, workerWordCount, (void *)&args[idx]);
  }

  // wait for all threads to be done
  for (int idx = 0; idx < NUMTHREADS; ++idx) {
    pthread_join(workers[idx], NULL);
  }
  
  return count;
}
