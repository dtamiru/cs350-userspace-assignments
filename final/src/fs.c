#include "disk.h"
#include "fs.h"

#include <stdio.h>
#include <string.h>
#include <math.h>
#include <stdlib.h>

#define min(a,b) (((a) < (b)) ? (a) : (b))

#ifdef DEBUG
#define DEBUG_PRINT(fmt, args...)    fprintf(stderr, fmt, ## args)
#else
#define DEBUG_PRINT(fmt, args...)    /* Don't do anything in release builds */
#endif

// Debug file system -----------------------------------------------------------

bool invalidBlocknum(ssize_t blocknum, Disk *disk) {
    return disk && (blocknum < 0 || blocknum >= disk->Blocks);
}

void fs_debug(Disk *disk)
{
    if (disk == 0)
        return;

    Block block;

    // Read Superblock
    disk_read(disk, 0, block.Data);

    uint32_t magic_num = block.Super.MagicNumber;
    uint32_t num_blocks = block.Super.Blocks;
    uint32_t num_inodeBlocks = block.Super.InodeBlocks;
    uint32_t num_inodes = block.Super.Inodes;

    if (magic_num != MAGIC_NUMBER)
    {
        printf("Magic number is valid: %c\n", magic_num);
        return;
    }

    printf("SuperBlock:\n");
    printf("    magic number is valid\n");
    printf("    %u blocks\n", num_blocks);
    printf("    %u inode blocks\n", num_inodeBlocks);
    printf("    %u inodes\n", num_inodes);

    uint32_t expected_num_inodeBlocks = round((float)num_blocks / 10);

    if (expected_num_inodeBlocks != num_inodeBlocks)
    {
        printf("SuperBlock declairs %u InodeBlocks but expect %u InodeBlocks!\n", num_inodeBlocks, expected_num_inodeBlocks);
    }

    uint32_t expect_num_inodes = num_inodeBlocks * INODES_PER_BLOCK;
    if (expect_num_inodes != num_inodes)
    {
        printf("SuperBlock declairs %u Inodes but expect %u Inodes!\n", num_inodes, expect_num_inodes);
    }

    // Scan Inode Blocks:

    for (int iblock = 0; iblock < num_inodeBlocks; iblock++) { // for each inode block
        disk_read(disk, 1 + iblock, block.Data);

        for (int i = 0; i < INODES_PER_BLOCK; i++) { // for each inode
            
            if (block.Inodes[i].Valid) {
                printf("Inode %d:\n", iblock * INODES_PER_BLOCK + i);  // calc inumber and print it
                printf("    size: %d bytes\n", block.Inodes[i].Size); // print size
                
                // scan direct pointers
                printf("    direct blocks:");
                for (int dblock = 0; dblock < POINTERS_PER_INODE; dblock++) {
                    if (block.Inodes[i].Direct[dblock] == 0) break;
                    printf(" %d", block.Inodes[i].Direct[dblock]);
                }
                printf("\n");
                
                
                // scan indirect pointers
                if (block.Inodes[i].Indirect != 0) { // if inode has indirect blocks
                    printf("    indirect block: %d\n", block.Inodes[i].Indirect);
                    printf("    indirect data blocks:");
                    
                    Block indirect_block;
                    disk_read(disk, block.Inodes[i].Indirect, indirect_block.Data);

                    for (int ind = 0; ind < POINTERS_PER_BLOCK; ind++) {
                        if (indirect_block.Pointers[ind] == 0) break;
                        printf(" %d", indirect_block.Pointers[ind]);
                    }
                    printf("\n");
                }
            }
        }
        
    }
}

// Format file system ----------------------------------------------------------

bool fs_format(Disk *disk)
{
    if (!disk || disk_mounted(disk))
        return false; // disk is null or already mounted

    Block block = { 
        .Super.MagicNumber = MAGIC_NUMBER,
        .Super.Blocks = disk->Blocks,
        .Super.InodeBlocks = round((float)disk->Blocks/10),
        .Super.Inodes = INODES_PER_BLOCK * round((float)disk->Blocks/10)
    };

    // Write superblock
    disk_write(disk, 0, block.Data);

    /*  Clear all other blocks. Though less effecient, zero out all
        blocks after super block. This also sets all valid bits to false  */
    Block zeroBlock = {.Data = {0}};

    for (int block_idx = 1; block_idx < disk->Blocks; block_idx++)
        disk_write(disk, block_idx, zeroBlock.Data);

    return true;
}

// FileSystem constructor 
FileSystem *new_fs()
{
    FileSystem *fs = malloc(sizeof(FileSystem));
    return fs;
}

// FileSystem destructor 
void free_fs(FileSystem *fs)
{
    free(fs->freeBlockBitmap);
    free(fs);
}

// Mount file system -----------------------------------------------------------

void initialize_free_blocks(FileSystem *fs, Disk *disk) {
    Block block;
    disk_read(disk, 0, block.Data);

    uint32_t num_blocks = block.Super.Blocks;
    uint32_t num_inodeBlocks = block.Super.InodeBlocks;

    // Allocate free block bitmap
    fs->freeBlockBitmap = malloc(num_blocks * sizeof(bool));
    
    // Fill free block bitmap
    for (int b = 0; b < num_blocks; b++) // set blocks free by default
        fs->freeBlockBitmap[b] = true;

    fs->freeBlockBitmap[0] = false; // superblock not free

    for (int i = 1; i < 1 + num_inodeBlocks; i++) {
        fs->freeBlockBitmap[i] = false; // inode blocks not free
    
        disk_read(disk, i, block.Data);

        for (int inode_idx = 0; inode_idx < INODES_PER_BLOCK; inode_idx++) { // for each inode
            
            if (block.Inodes[inode_idx].Valid) {
                // check direct pointers 
                for (int dir_idx = 0; dir_idx < POINTERS_PER_INODE; dir_idx++) { // for each direct ptr

                    uint32_t dir_ptr = block.Inodes[inode_idx].Direct[dir_idx];
                    fs->freeBlockBitmap[dir_ptr] = false;
                }
                
                // check indirect pointers 
                fs->freeBlockBitmap[block.Inodes[inode_idx].Indirect] = false; // indirect block not free

                if (block.Inodes[inode_idx].Indirect != 0) { // if there is an indirect block
                    Block indirect_block;
                    disk_read(disk, block.Inodes[inode_idx].Indirect, indirect_block.Data);

                    for (int ind_idx = 0; ind_idx < POINTERS_PER_BLOCK; ind_idx++) { // for each indirect ptr
                       
                        uint32_t ind_ptr = indirect_block.Pointers[ind_idx];
                        fs->freeBlockBitmap[ind_ptr] = false;
                    }
                }
            }
        }  
    }
}


bool fs_mount(FileSystem *fs, Disk *disk)
{
    if (fs == NULL || disk == NULL || disk_mounted(disk)) 
        return false;
    
    // Read Superblock
    Block block;
    disk_read(disk, 0, block.Data);

    uint32_t magic_num = block.Super.MagicNumber;
    uint32_t num_blocks = block.Super.Blocks;
    uint32_t num_inodeBlocks = block.Super.InodeBlocks;
    uint32_t num_inodes = block.Super.Inodes;

    // Validate metadata
    if (magic_num != MAGIC_NUMBER || 
        num_blocks != disk->Blocks ||
        num_inodeBlocks != round((float)disk->Blocks/10) ||
        num_inodes != INODES_PER_BLOCK * round((float)disk->Blocks/10)
    ) return false;
    
    // Set device and mount
    fs->disk = disk;
    disk_mount(disk);

    // Allocate and Fill free block bitmap
    initialize_free_blocks(fs, disk);

    return true;
}

// Create inode ----------------------------------------------------------------

ssize_t fs_create(FileSystem *fs)
{
    if (fs == NULL || fs->disk == NULL) 
        return -1;

    // get number of inode blocks
    Block block;
    disk_read(fs->disk, 0, block.Data);
    uint32_t num_inodeBlocks = block.Super.InodeBlocks;
  
    // Locate free (first invalid) inode in inode table
    for (int i = 0; i < num_inodeBlocks; i++) {
        disk_read(fs->disk, 1+i, block.Data);

        for (int inode_idx = 0; inode_idx < INODES_PER_BLOCK; inode_idx++) { // for each inode

            if (!block.Inodes[inode_idx].Valid) { // free inode found!
                // init inode metadata
                block.Inodes[inode_idx].Valid = true;
                block.Inodes[inode_idx].Size = 0;
                for (int d = 0; d < POINTERS_PER_INODE; d++) block.Inodes[inode_idx].Direct[d] = 0;
                block.Inodes[inode_idx].Indirect = 0;
                // write the inode to disk and return the inumber
                disk_write(fs->disk, 1+i, block.Data);
                return INODES_PER_BLOCK * i + inode_idx;
            }
        }
    }
    return -1;
}

// Remove inode ----------------------------------------------------------------

// Optional: the following two helper functions may be useful. 

bool find_inode(FileSystem *fs, size_t inumber, Inode *inode)
{
    Block block;
    disk_read(fs->disk, 0, block.Data);
    uint32_t num_inodeBlocks = block.Super.InodeBlocks;

    // compute inode's block and corresponding index
    size_t blocknum = 1 + inumber / INODES_PER_BLOCK;
    size_t inode_idx = inumber % INODES_PER_BLOCK;

    if (blocknum > num_inodeBlocks) return false;
  
    disk_read(fs->disk, blocknum, block.Data); // read its block

    // copy data out of block
    if (!block.Inodes[inode_idx].Valid)
        return false;

    inode->Valid = block.Inodes[inode_idx].Valid;
    inode->Size = block.Inodes[inode_idx].Size;
    for (int dir = 0; dir < POINTERS_PER_INODE; dir++)
        inode->Direct[dir] = block.Inodes[inode_idx].Direct[dir];
    inode->Indirect = block.Inodes[inode_idx].Indirect;

    return true;
}

bool store_inode(FileSystem *fs, size_t inumber, Inode *inode)
{
    Block block;
    disk_read(fs->disk, 0, block.Data);
    uint32_t num_inodeBlocks = block.Super.InodeBlocks;

    // compute inode's block and corresponding index
    size_t blocknum = 1 + inumber / INODES_PER_BLOCK;
    size_t inode_idx = inumber % INODES_PER_BLOCK;

    if (blocknum > num_inodeBlocks) return false;
  
    disk_read(fs->disk, blocknum, block.Data); // read its block

    // copy data into block
    block.Inodes[inode_idx].Valid = inode->Valid;
    block.Inodes[inode_idx].Size = inode->Size;
    for (int d = 0; d < POINTERS_PER_INODE; d++)
        block.Inodes[inode_idx].Direct[d] = inode->Direct[d];
    block.Inodes[inode_idx].Indirect = inode->Indirect;

    // write (store) inode to disk
    disk_write(fs->disk, blocknum, block.Data);

    return true;
}


bool fs_remove(FileSystem *fs, size_t inumber)
{
    // Load inode information
    Inode inode;
    if (!find_inode(fs, inumber, &inode))
        return false;

    // Free direct blocks
    for (int d = 0; d < POINTERS_PER_INODE; d++) {
        if (inode.Direct[d] != 0)
            fs->freeBlockBitmap[inode.Direct[d]] = true;

        inode.Direct[d] = 0; // free direct block pointer
    }
        
    // Free indirect blocks
    if (inode.Indirect != 0) {
        fs->freeBlockBitmap[inode.Indirect] = true; // free indirect block itself

        Block indirect_block;
        disk_read(fs->disk, inode.Indirect, indirect_block.Data);
        inode.Indirect = 0; // free indirect pointer

        for (int i = 0; i < POINTERS_PER_BLOCK; i++) { // for each indirect ptr        
            uint32_t ind_ptr = indirect_block.Pointers[i];
            fs->freeBlockBitmap[ind_ptr] = false;
            
            indirect_block.Pointers[i] = 0; // free indirect data block pointer
        }
    }
   
    // Clear inode in inode table
    inode.Valid = false;
    if (!store_inode(fs, inumber, &inode))
        return false;

    return true;
}

// Inode stat ------------------------------------------------------------------

ssize_t fs_stat(FileSystem *fs, size_t inumber)
{   
    Inode inode;
    if (!find_inode(fs, inumber, &inode))
        return -1;

    return inode.Size;
}

// Read from inode -------------------------------------------------------------

ssize_t fs_read(FileSystem *fs, size_t inumber, char *data, size_t length, size_t offset)
{
    Block block, indirect_block;

    // Load inode information
    Inode inode;
    if (!find_inode(fs, inumber, &inode))
        return -1;

    // Adjust length
    size_t end = min(offset + length, inode.Size);
    length = end - offset;
    
    // Read block and copy to data
    size_t pos = offset;

    // DIRECT DATA BLOCKS:
    for (size_t dir = pos/BLOCK_SIZE; dir < POINTERS_PER_INODE && pos < end; dir++) { // for each direct data block
    
        disk_read(fs->disk, inode.Direct[dir], block.Data); // read block

        for (size_t block_pos = pos % BLOCK_SIZE; pos/BLOCK_SIZE == dir && pos < end; pos++) 
            data[pos-offset] = block.Data[block_pos++]; // copy data
    }
    
    if (pos >= end) return length; // if done with just direct data blocks, return
    size_t dir_end = POINTERS_PER_INODE * BLOCK_SIZE;

    // INDIRECT DATA BLOCKS:
    disk_read(fs->disk, inode.Indirect, indirect_block.Data);

    for (size_t ind = (pos - dir_end)/BLOCK_SIZE; ind < POINTERS_PER_BLOCK && pos < end; ind++) { // for each indirect data block
    
        disk_read(fs->disk, indirect_block.Pointers[ind], block.Data); // read block

        for (size_t block_pos = pos % BLOCK_SIZE; (pos - dir_end)/BLOCK_SIZE == ind && pos < end; pos++)
            data[pos-offset] = block.Data[block_pos++]; // copy data
    }

    return length;
}

// Optional: the following helper function may be useful. 

ssize_t fs_allocate_block(FileSystem *fs) {
    // find first free block
    for (int b = 0; b < fs->disk->Blocks; b++) { 
        if (fs->freeBlockBitmap[b] == true) {
            fs->freeBlockBitmap[b] = false;
            return b;
        }
    }

    return -1;
}

// Write to inode --------------------------------------------------------------

ssize_t fs_write(FileSystem *fs, size_t inumber, char *data, size_t length, size_t offset)
{
    // check if mounted
    if (!fs || !fs->disk || !disk_mounted(fs->disk))
        return -1;

    Block block, indirect_block;

    // Load inode information
    Inode inode;
    if (!find_inode(fs, inumber, &inode))
        return -1;

    /* compute 'end' (tentatively), the last byte written to + 1.
    / If we run out of space, end will be changed accoringly */
    size_t cap = (POINTERS_PER_INODE + POINTERS_PER_BLOCK) * BLOCK_SIZE;
    size_t end = min(offset + length, cap);
    
    size_t pos = offset; // init position


    // DIRECT DATA BLOCKS:
    for (size_t dir = pos/BLOCK_SIZE; dir < POINTERS_PER_INODE && pos < end; dir++) { // for each direct data block

        if (inode.Direct[dir] == 0) { // if required direct block doesn't exist
            ssize_t new_block = fs_allocate_block(fs);

            if (invalidBlocknum(new_block, fs->disk)) { // if out of blocks
                end = pos; break;
            }
            inode.Direct[dir] = new_block;
        }
           
        disk_read(fs->disk, inode.Direct[dir], block.Data); // read block

        for (size_t block_pos = pos % BLOCK_SIZE; pos/BLOCK_SIZE == dir && pos < end; pos++) // copy data
            block.Data[block_pos++] = data[pos-offset];
        
        disk_write(fs->disk, inode.Direct[dir], block.Data); // write block
    }

    if (pos >= end) { // if done with just direct data blocks, return
        inode.Size = end;
        if (!store_inode(fs, inumber, &inode)) return -1; // Write inode back with updated metadata
        return end - offset; 
    }


    // INDIRECT DATA BLOCKS:
    size_t dir_end = POINTERS_PER_INODE * BLOCK_SIZE;

    if (inode.Indirect != 0) {  // if inode has indirect block data
        disk_read(fs->disk, inode.Indirect, indirect_block.Data);
    }  
    else { // otherwise
        ssize_t new_block = fs_allocate_block(fs);
        
        if (invalidBlocknum(new_block, fs->disk)) end = pos; // if out of blocks
        else { // block is a available
            inode.Indirect = new_block;
            disk_read(fs->disk, inode.Indirect, indirect_block.Data);

            for (size_t ind = 0; ind < POINTERS_PER_BLOCK; ind++) // zero out all pointers
                indirect_block.Pointers[ind] = 0;
        }
    }
        
    for (size_t ind = (pos - dir_end)/BLOCK_SIZE; ind < POINTERS_PER_BLOCK && pos < end; ind++) { // for each indirect data block
        if (indirect_block.Pointers[ind] == 0) { // if required indirect block doesn't exist
            ssize_t new_block = fs_allocate_block(fs);

            if (invalidBlocknum(new_block, fs->disk)) {// if out of blocks
                end = pos; break;
            }
            indirect_block.Pointers[ind] = new_block;
        } 

        disk_read(fs->disk, indirect_block.Pointers[ind], block.Data); // read block

        for (size_t block_pos = pos % BLOCK_SIZE; (pos - dir_end)/BLOCK_SIZE == ind && pos < end; pos++) // copy data
            block.Data[block_pos++] = data[pos-offset];

        disk_write(fs->disk, indirect_block.Pointers[ind], block.Data); // write block
    }

    if (inode.Indirect != 0) disk_write(fs->disk, inode.Indirect, indirect_block.Data); // wite indirect block back (if it was allocated in the first place)

    inode.Size = end;
    if (!store_inode(fs, inumber, &inode)) return -1; // Write inode back with updated metadata
    return end - offset;
}
