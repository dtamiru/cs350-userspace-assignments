/* main.c
 * ----------------------------------------------------------
 *  CS350
 *  Midterm Programming Assignment
 *
 *  Purpose:  - Use Linux programming environment.
 *            - Review process creation and management
 * ----------------------------------------------------------
 */

#include <stdbool.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <stdio.h>
#include <sys/types.h>
#include<sys/wait.h>

// Static Constants
const char* promptStr = "> ";
const char error_message[30] = "An error has occurred\n";
const MAX_LINE_SIZE = 512;
const MAX_PATH_SIZE = 4096;

// Static Variables
bool inBatchMode = false;
char* cmd[MAX_LINE_SIZE]; int cmdLen = 0;

// =====================

void printError() {
  write(STDERR_FILENO, error_message, strlen(error_message));
}


FILE* handleArgs(int argc, char **argv) {
  if(argc > 2) {// validate argument count
    printError(); exit(1);
  }
  
  FILE* inputFile = stdin; // get input file

  if(argc == 2) { // set program to batch mode
    inBatchMode = true;
    inputFile = fopen(argv[1], "r");
    if(!inputFile) {
      printError(); exit(1);
    }
  } 
    
  return inputFile;
}


void parseCmd(FILE* inputFile) {
  for(int i = 0; i < MAX_LINE_SIZE; i++)  // clean the cmd buffer
    cmd[i] = NULL;
 
  // get line of input
  char line[MAX_LINE_SIZE];
  if(!fgets(line, MAX_LINE_SIZE, inputFile))
    exit(1);

  line[strcspn(line, "\n")] = 0; // remove \n from the input

  char* cmdArg = strtok(line, " "); // the first command arg is the name of the program

  for (cmdLen = 0; cmdArg != NULL; cmdLen++) {
      cmd[cmdLen] = cmdArg;
      cmdArg = strtok(NULL, " ");
   }
}


// Built-in Commands:

void cd() {
  if(cmdLen == 1) // no path - go to home directory
    chdir(getenv("HOME"));
  else if (cmdLen == 2) // follow path
    chdir(cmd[1]);
  else
    printError();
}

void pwd() {
  if (cmdLen != 1) {
    printError();
  } 
  else {
    char path[MAX_PATH_SIZE];
    getcwd(path, MAX_PATH_SIZE); // get path
    strcat(path, "\n");

    write(STDOUT_FILENO, path, strlen(path)); // print the wd's path
  }
}

void waitjob() {
  wait(NULL); // wait for all child procs to exit
}

void help() {
  // print a list of the built-in commands
  write(STDOUT_FILENO, 
    "cd\npwd\nwait\nexit\nhelp\n", 
    strlen("cd\npwd\nwait\nexit\nhelp\n")
  );
}


// ============= MAIN =================

int main(int argc, char ** argv) {
 
  FILE* inputFile = handleArgs(argc, argv);

  while(1) {
    
    if (!inBatchMode) { // print prompt if not in batch mode
      write(STDOUT_FILENO, promptStr, strlen(promptStr));
    }

    parseCmd(inputFile); // parse cmd lines

    // background job check
    bool isBackgroundJob = false;
    if(!(strcmp(cmd[cmdLen-1], "&"))) { 
      isBackgroundJob = true;
      cmd[--cmdLen] = NULL; 
    }

    // check if cmd is built-in
    if (!(strcmp(cmd[0], "cd"))) 
      cd();
    else if (!(strcmp(cmd[0], "pwd"))) 
      pwd();
    else if (!(strcmp(cmd[0], "wait"))) 
      waitjob();
    else if (!(strcmp(cmd[0], "exit"))) 
      break;
    else if (!(strcmp(cmd[0], "help")))
      help();

    else { // command is not built-in
      pid_t pid = fork();

      switch(pid) {
        case -1:
          printError(); exit(1);

        case 0: { // child proc: execute 
          if(execvp(cmd[0], cmd) < 0)
            printError(); exit(1);
        }

        default: { // parent proc: wait (if not a background job)
          if(!isBackgroundJob && waitpid(pid, NULL, 0) == -1)
            printError(); exit(1);
        }
      }
    }
  }
  
  fclose(inputFile);
  return 0;
}
