#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <sys/wait.h>
#include <unistd.h>
#include <fcntl.h>

// Global Constants
const char* promptString = "$ ";
const int NONE = -1;

void execCmd(char** cmdArgs) {
    // REDIRECTION

    char* inFile = ""; char* outFile = "";
    int argIdx = 0; int redirectionIdx = NONE;
    // Find redirection operators (if they exist) and marks leftmost one as NULL (the point to which execv operates on)
    while (cmdArgs[argIdx]) {
        if (strcmp(cmdArgs[argIdx], "<") == 0) {  // Input redirect found
            if (redirectionIdx == NONE)
                redirectionIdx = argIdx;
            inFile = cmdArgs[++argIdx]; // get infiles
        } 
        else if (strcmp(cmdArgs[argIdx], ">") == 0) { // Output redirect found
            if (redirectionIdx == NONE)
                redirectionIdx = argIdx;
            outFile = cmdArgs[++argIdx]; // get outfile
        }
        ++argIdx;
    }
    // Mark primary redirection argument as null so that execv knows where to stop
    if (redirectionIdx != NONE) cmdArgs[redirectionIdx] = NULL;

    int fd;
    if (inFile[0] != '\0') { // if infile was provided
        if (0 > (fd = open(inFile, O_RDONLY))) { // file is read-only
            perror(inFile); _exit(1);
        } if (fd != 0) {
            dup2(fd, STDIN_FILENO); close(fd);
        }
    }

    if (outFile[0] != '\0') { // if outfile was provided
        if ((fd = open(outFile, O_WRONLY)) < 0) { // file is write only
            perror(outFile); _exit(1);
        } if (fd != 0) {
            dup2(fd, STDOUT_FILENO); close(fd);
        }
    }

    // PIPING

    argIdx = 0;
    int procs = 1;

    for (; cmdArgs[argIdx]; argIdx++) {
        if (strcmp(cmdArgs[argIdx], "|") == 0) { // pipe!
            // Create pipe ends                 
            int pipefds[2];
            pipe(pipefds);

            // Fork process
            switch (fork()) {
                case 0: // Child
                    dup2(pipefds[0], STDIN_FILENO); // stdin -> piple left arg
                    close(pipefds[1]); close(pipefds[0]);
                    cmdArgs += argIdx + 1; // offset command args
                    argIdx = NONE;
                    break;
                case -1: // Error
                    perror("fork"); _exit(1);
                default:        
                    dup2(pipefds[1], STDOUT_FILENO); // stdout -> pipe right arg
                    close(pipefds[1]); close(pipefds[0]);
                    procs = 0;
                    // set stopping point for execvp
                    cmdArgs[argIdx] = NULL; 
                    break;
            }
        }
        // Parent processes will be able to break out of while loop
        if (procs == 0) break;
    }


    // Explanation:
    // exectutes normally with no pipes - gets executable, args (converting them if they're env vars) and the exec (execvp) function turns the current process into the desired proc
    // with each pipe, we fork one level further. the forked shell proc set left side of | as input and executes it, while original shell runner sets the right side of | as dtouut and executes it afterwards

    // EXECUTE
    
    execvp(cmdArgs[0], cmdArgs);
    perror(cmdArgs[0]); 
   _exit(1);
}


char** parseCmd() {
    char** cmdArgs = NULL; size_t argNum = 0;
    char* line = NULL; size_t lineLen;

    if (getline(&line, &lineLen, stdin) != EOF) {
        char* arg = strtok(line, " ");

        while(arg) { // split cmd line into arguments
            cmdArgs = realloc(cmdArgs, ++argNum * sizeof(char*));
            cmdArgs[argNum - 1] = arg;
            arg = strtok(NULL, " ");
        }
        
        cmdArgs = realloc(cmdArgs, (argNum + 1) * sizeof(char*));
        cmdArgs[argNum] = NULL; // terminate array with null

        int len = strlen(cmdArgs[argNum - 1]);
        if (cmdArgs[argNum - 1][len - 1] == '\n')
            cmdArgs[argNum - 1][len - 1] = 0; // Remove trailing newlines
    }
    return cmdArgs;
} 


// ############ MAIN ###############

int main(int argc, char* argv[]) {
    while (1) {
        printf("%s", promptString); // print prompt
        char** cmdArgs = parseCmd(); // get command arguments

        if (cmdArgs == NULL) break; // EOF reached!

        // Fork -> Run cmd as child process
        pid_t pid = fork();
        switch (pid) {
        case 0:  // Cmd (child)         
            execCmd(cmdArgs);
        case -1: // Error        
            perror("fork");
            break;
        default: // Shell (parent)                     
            waitpid(pid, NULL, 0);
            break;
        }
        
        free(cmdArgs);
    }
}