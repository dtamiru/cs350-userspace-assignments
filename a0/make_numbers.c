#include "stdlib.h"
#include "stdio.h"   
#include "sys/time.h"

const int INT_MAX = 2147483647;
const int INT_MIN = -2147483648;
const int INVALID = 1;

int validateInput(int n, int lo, int hi) {
    if (n <= 0 || n > INT_MAX) {
        fprintf(stderr, "Error: input size should be positive and fit within INT size constraints");
        return 1;
    } else if (hi > INT_MAX || hi < INT_MIN) {
        fprintf(stderr, "Error: hi must fit within INT size constraints.\n");
        return 1;
    } else if (lo > INT_MAX || lo < INT_MIN) {
        fprintf(stderr, "Error: lo must fit within INT size constraints.\n");
        return 1;
    } else if (lo > hi) {
        fprintf(stderr, "Error: lo must be less than or equal to hi.\n");
        return 1;
    }
    return 0;
}

int getRandomNumber() {
    int num = 0;
    for (int bit = 0; bit < 32; bit++) {
        // randomly bit 'bit' to either 0 or 1
        num ^= (-(unsigned long)(rand() % 2) ^ num) & (1UL << bit);
    }
    return num;
}

int modulo(int n, int m) {
    int remainder = n % m;
    return remainder < 0 ? remainder + m : remainder;
}

int main(int argc, char *argv[]) {
    // Validate Number of Arguments
    if (argc != 4) {
        fprintf(stderr, "Requires three args: \"n\", \"lo\", and \"hi\".\n");
        return INVALID;
    }

    struct timeval seed; gettimeofday(&seed, NULL);
    srand(seed.tv_usec); // set random number seed
    
    // get input
    int n = atoi(argv[1]);
    int lo = atoi(argv[2]);
    int hi = atoi(argv[3]);
    if (validateInput(n, lo, hi) == INVALID)
        return INVALID;

    // output n random numbers in [lo, hi] to log.txt
    FILE *logFile = fopen("log.txt","w");

    fprintf(logFile, "%d\n", n);
    for (int i = 0; i < n; i++) {
        int num = lo + modulo(getRandomNumber(), hi - lo + 1);
        fprintf(logFile, "%d\n", num);
    }

    fclose(logFile);
}