#include "stdio.h"  
#include "stdlib.h"
#include "sys/time.h"

void merge(int *arr, int* L, int *R, int front, int back) {
    int middle = front + (back - front) / 2;
    int i = front;
    int l = front, r = middle + 1;

    // copy left and right subarrays
    for (int n = l; n <= middle; n++)
        L[n] = arr[n];
    for (int n = r; n <= back; n++)
        R[n] = arr[n];
    // merge them in order 
    for(; i <= back && l <= middle && r <= back; i++) {
        if(L[l] <= R[r])
            arr[i] = L[l++];
        else
            arr[i] = R[r++];
    }
    // append remainder of left or right subarray
    while(l <= middle) arr[i++] = L[l++];
    while(r <= back) arr[i++] = R[r++];
}

void mergeSort(int *arr, int *L, int *R, int front, int back) {
    if (front >= back) return;
    int middle = front + (back - front) / 2;
   
    mergeSort(arr, L, R, front, middle);
    mergeSort(arr, L, R, middle + 1, back);
    merge(arr, L, R, front, back);
}


int main(int argc, char *argv[]) {
    struct timeval start, end;

    // read from log file
    FILE *logFile = fopen("./log.txt","r");
    if (logFile == NULL) { // validate logFile:
        fprintf(stderr, "Error: 'log.txt' does not exist in your working directory");
        return 1;
    }

    // read number of values, n
    int n; 
    if (fscanf(logFile, "%d", &n) != 1) { // validate n is read
        fprintf(stderr, "Could not read n, the number of values");
        return 1;
    } else if (n <= 0) { // validate n is positive
        fprintf(stderr, "Error: number of subsequent numbers in 'log.txt' should be positive");
        return 1;
    }

    int nums[n];
    int L[n]; // Working Left subarray
    int R[n]; // Working Right subarray

    // Read Input
    for (int i = 0; i < n; i++) {
        int num; fscanf(logFile, "%d\n", &num);
        nums[i] = num;
    }

    // get start time
    struct timeval startTime;
    gettimeofday(&startTime, NULL);

    // Sort
    gettimeofday(&start, NULL);
    mergeSort(nums, L, R, 0, n-1);
    gettimeofday(&end, NULL);
    
    // write sorted array to sorted.txt
    FILE *sortFile = fopen("sorted.txt", "w");
    if (sortFile == NULL) { // validate sortFile:
        fprintf(stderr, "Error: could not open 'sorted.txt'");
        return 1;
    }
    fprintf(sortFile, "%d\n", n);
    for (int i = 0; i < n; i++) {
        int num = nums[i];
        fprintf(sortFile, "%d\n", num);
    }

    // Close files
    fclose(logFile); 
    fclose(sortFile);

    // output elapsed time
    printf("%ld\n", 1000000 * (end.tv_sec - start.tv_sec) + end.tv_usec - start.tv_usec);
}